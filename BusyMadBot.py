# -*- coding: utf-8 -*-
import json
import requests
import time
import urllib
import urllib2
import math
import datetime
# Backdoor, para apagar y reiniciar el bot 1/2
import os
import pickle
# 

from BusyMadBotkeys import TOKEN, idClient, passKey, shutdown, reboot, on #Las contraseñas, lo único privado de mi bot ;)
URL = "https://api.telegram.org/bot{}/".format(TOKEN)


def bus(parada):

    # Esta parte del código está suministrada por la EMT, que nos lo pone fácil enviándonos los datos en formato JSON.
    # Lo tenéis aquí: https://github.com/esri-es/mobilitylab-emtmadrid/blob/master/openbus/openbus-get-arrive-stop.py
    urlGetFindBusTest="https://openbus.emtmadrid.es:9443/emt-proxy-server/last/geo/GetArriveStop.php"

    useragent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'

    params = {
      'idClient' : idClient,
      'passKey' : passKey,
      'idStop' : parada
    }

    headers = { 'User-Agent' : useragent,'Content-type':'application/x-www-form-urlencoded' }

    data = urllib.urlencode(params)

    req = urllib2.Request(urlGetFindBusTest, data, headers)

    response = urllib2.urlopen(req)

    results = response.read()

    resultJson = json.loads(results)
    # Hasta aqui el codigo de EMT Open Data

    resultJsonbus = resultJson['arrives']
    llegadas='' # llegadas no es una lista, pero como si lo fuera
    for results in resultJsonbus:
        tiempomin=int(math.floor(results['busTimeLeft']/60)) #parte minutos del tiempo. Redondeamos a la baja (ideal para impuntuales) y no incluimos segundos (la estimacion no tiene realmente tanta precision)

        if results['busTimeLeft']>30*60:
            tiempomin= 'más de 20'# para cuando nos dan valores indeterminados del tiempo
            
        results['destination']=results['destination'].encode('utf-8') #para evitar problemas con las eñes, no sé por qué ASCII es el default
        llegadas=llegadas + '\n<b>{}</b> en <b>{}</b> min. \n<i>{}, {} m</i>\n'.format( results['lineId'] ,tiempomin,results['destination'],results['busDistance'])
        
    print datetime.datetime.now(), 'Petición respondida'
    return llegadas

    
    
# La mayoría de las siguientes funciones son versiones (casi todas muy modificadas para adaptarlas a nuestro caso) de las de Gareth Dwyer, autor de un genial tutorial sobre bots de Telegram. 
# Aquí tenéis las originales: https://github.com/sixhobbits/python-telegram-tutorial/blob/master/part1/echobot.py
def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content
def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js
def get_updates(offset=None):
    url = URL + "getUpdates"
    if offset:
        url += "?offset={}".format(offset)
    js = get_json_from_url(url)
    return js
def get_last_update_id(updates):
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)

def echo_all(updates):

    for update in updates["result"]:

        # Estos trys no son "pasa-errores", sirven para determinar si el result es un mensaje o una callback_query
        try: 
            update["message"]
            tipo='message'
        except: pass
        try:
            update["callback_query"]
            tipo='callback_query'
        except: pass
        # 

        if tipo=='message':
            chat = update["message"]["chat"]["id"]
            text = update["message"]["text"]
            switch= pickle.load( open( "switch.p", "rb" ) ) #puerta trasera
            
            # Backdoor, para apagar y reiniciar el bot 2/2
            if text==reboot and switch==1:
            	send_message('Reiniciando...', chat,'666')
                switch=0
                pickle.dump(switch, open( "switch.p", "wb" ) )
            	os.system('sudo reboot now')
            if text==shutdown and switch==1:
                send_message('Apagando...',chat,'666')
                switch=0
                pickle.dump(switch, open( "switch.p", "wb" ) )
                os.system('sudo shutdown now')
            if text==on: # encendemos el interruptor que nos permite apagarlo
                switch=1
                pickle.dump(switch, open( "switch.p", "wb" ) )
            # 

            llegadas=bus(text)
            send_message(llegadas, chat,text)

        elif tipo=='callback_query':
            data=update["callback_query"]["data"]
            ide=update["callback_query"]["id"] #no, no debemos llamarlo id
            chat=update["callback_query"]["message"]["chat"]["id"] #ver funcion edit_message_text
            answer_callback_query(ide)
            # edit_message_text(chat) #ver funcion edit_message_text
            llegadas=bus(data)
            send_message(llegadas,chat,data)


def send_message(text, chat_id,parada):
    url = URL + "sendMessage?text={}&chat_id={}&parse_mode=HTML".format(text, chat_id)
    url= url + '&reply_markup='+json.dumps({'inline_keyboard': [[ {'text': u'\U0001F6CE', 'callback_data': parada}]]})
    get_url(url)

def answer_callback_query(ide): 
    mensaje='Tiempo refrescado'
    url= URL + "answerCallbackQuery?callback_query_id={}&text={}".format(ide,mensaje)
    get_url(url)

#  Mi objetivo era actualizar los tiempos editando el propio mensaje original, en vez de enviar uno nuevo
#  Por desgracia, Telegram aún no soporta esa acción (como avisan encima de https://core.telegram.org/bots/api#editmessagetext). Tengo esperanza de que lo habiliten algún día.
# 
# def edit_message_text(ide): #propia
#     nuevo='prueba' #mensaje editado
#     url= URL + "editMessageText?chat_id={}&text={}".format(ide, nuevo)
#     print 'si'
#     get_url(url)

def main():
    last_update_id = None

    while True:
        # Uso irresponsablemente los trys para librarme de errores como texto o códigos de parada incorrectos. 
        #  Sé que no es una buena práctica y que debería hacer que el bot se quejara, pero sería añadir demasiadas líneas al código y confío en la inteligencia del usuario.
        try:
            updates = get_updates(last_update_id)
            if len(updates["result"]) > 0:
                last_update_id = get_last_update_id(updates) + 1
                echo_all(updates)
 
        except Exception as e: 
            print 'error:', e

        time.sleep(0.05) 

if __name__ == '__main__':
    main()
